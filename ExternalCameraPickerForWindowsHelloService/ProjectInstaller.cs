﻿using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;

namespace ExternalCameraPickerForWindowsHelloService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        // Start the Service right after installation.
        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C sc start " + this.serviceInstaller1.ServiceName;

            Process process = new Process();
            process.StartInfo = startInfo;
            process.Start();
        }
    }
}
