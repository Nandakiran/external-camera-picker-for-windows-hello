﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Windows;

namespace ExternalCameraPickerForWindowsHelloService
{
    public partial class ExternalCameraPickerForWindowsHelloService : ServiceBase
    {
        private delegate void HandlerEx(int control, int eventType, IntPtr eventData, IntPtr context);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr RegisterPowerSettingNotification(IntPtr hRecipient, ref Guid PowerSettingGuid,
            Int32 Flags);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool UnregisterPowerSettingNotification(IntPtr handle);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern IntPtr RegisterServiceCtrlHandlerEx(string lpServiceName, HandlerEx cbex, IntPtr context);

        internal struct POWERBROADCAST_SETTING
        {
            public Guid PowerSetting;
            public uint DataLength;
            public byte Data;
        }

        static Guid GUID_LIDSWITCH_STATE_CHANGE = new Guid(0xBA3E0F4D, 0xB817, 0x4094, 0xA2, 0xD1, 0xD5, 0x63, 0x79, 0xE6, 0xA0, 0xF3);
        const int DEVICE_NOTIFY_SERVICE_HANDLE = 0x00000001;
        const int PBT_POWERSETTINGCHANGE = 0x8013;
        
        const int SERVICE_CONTROL_STOP = 0x00000001;
        const int SERVICE_CONTROL_SHUTDOWN = 0x00000005;
        const int SERVICE_CONTROL_POWEREVENT = 0x0000000D;
        
        private bool? _previousLidState = null;
        private IntPtr hMonitorOn = IntPtr.Zero;
        private List<string> devices;

        public ExternalCameraPickerForWindowsHelloService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var settings = ConfigurationManager.AppSettings["devices"];
            if (settings != null)
                this.devices = new List<string>(settings.Split(new char[] { ';' }));
            else
                this.devices = new List<string>();
            RegisterForPowerNotifications();
            Debug.Print("Service started with configuration : {0}", this.devices);
            // TODO: Get Current Lid state and do initial action.
        }


        private void ShutOffService()
        {
            Debug.WriteLine("Shutting Down Service");
            if (this.hMonitorOn != IntPtr.Zero)
            {
                Debug.Print("Shutting hMonitor");
                UnregisterPowerSettingNotification(this.hMonitorOn);
                Debug.Print("UnregisterPowerSettingNotification error code : {0}", Marshal.GetLastWin32Error());
            }
            base.Stop();
        }

        private void RegisterForPowerNotifications()
        {
            // This will take over the the service callbacks and disable all calls except OnStart()
            // which is called before we register for the handler.
            var serviceStatusHandle = RegisterServiceCtrlHandlerEx(this.ServiceName, DoHandlerCallback, IntPtr.Zero);
            this.hMonitorOn = RegisterPowerSettingNotification(serviceStatusHandle, ref GUID_LIDSWITCH_STATE_CHANGE, DEVICE_NOTIFY_SERVICE_HANDLE);
        }

        private void DoHandlerCallback(int control, int eventType, IntPtr eventData, IntPtr context)
        {
            Debug.Print("In DoHandlerCallback. Control {0}: EventType {1}", control, eventType);
            switch (control)
            {
                case SERVICE_CONTROL_POWEREVENT:
                    switch (eventType)
                    {
                        case PBT_POWERSETTINGCHANGE:
                            if (eventData != null)
                            {
                                POWERBROADCAST_SETTING ps = (POWERBROADCAST_SETTING)Marshal.PtrToStructure(eventData, typeof(POWERBROADCAST_SETTING));
                                if (ps.PowerSetting == GUID_LIDSWITCH_STATE_CHANGE)
                                {
                                    Debug.Print("Got LidSwitch State change notification.");
                                    bool isLidOpen = ps.Data != 0;
                                    if (!isLidOpen == _previousLidState)
                                    {
                                        LidStatusChanged(isLidOpen);
                                    }
                                    _previousLidState = isLidOpen;
                                }
                            }
                            break;
                        default:
                            Debug.Print("Unknown EventType {0} in PowerEvent",eventType);
                            break;
                    }
                    break;
                case SERVICE_CONTROL_SHUTDOWN:
                case SERVICE_CONTROL_STOP:
                    ShutOffService();
                    break;
                default:
                    Debug.Print("Unknown Control code : {0}", control);
                    break;
            }
        }
        private void LidStatusChanged(bool isLidOpen)
        {
            Debug.WriteLine("Lid status changed..");
            if (isLidOpen)
            {
                foreach (var device in this.devices)
                {
                    Debug.Print("Enabling device : {0}", device);
                    DeviceManager.SetDeviceStatus(n => n.ToUpperInvariant().Contains(device), /*disable*/ false);
                }
            }
            else
            {
                foreach (var device in this.devices)
                {
                    Debug.Print("Disabling device : {0}", device);
                    DeviceManager.SetDeviceStatus(n => n.ToUpperInvariant().Contains(device), /*disable*/ true);
                }
            }
        }
    }
}
