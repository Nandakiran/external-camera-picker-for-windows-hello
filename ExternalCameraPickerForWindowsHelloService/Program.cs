﻿using System.ServiceProcess;

namespace ExternalCameraPickerForWindowsHelloService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ExternalCameraPickerForWindowsHelloService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
