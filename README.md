Windows Hello tends to prefer the internal camera for laptops. When we have 
external Windows Hello compatible camera connected, it still picks the internal
camera. 

When using Docks, most folks tend to have thier laptops closed. This project 
creates a Windows Service component that listens to Lid closed / open events, 
and Disables/Enables in the Internal camera.

It also has a Windows Application which is used to configure the camera(s) that
should be enabled/disabled.