﻿using ExternalCameraPickerForWindowsHelloService;
using System.Collections.Generic;
using System.Configuration;
using System.Windows;
using System.ServiceProcess;
using System;
using System.Diagnostics;

namespace ExternalCameraPickerForWindowsHello
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            RefreshDeviceList();
            this.btnSave.Click += (sender, args) => { Save(); };
        }

        private void RefreshDeviceList()
        {
            var cameras = DeviceManager.ListDevices("Camera");

            // Clear the ListBox
            this.lstDevices.ItemsSource = cameras;
        }
        void Save()
        {
            Debug.WriteLine("Saving configuration...");
            var devices = new List<string>();

            foreach (var item in this.lstDevices.SelectedItems)
            {
                devices.Add(((KeyValuePair<string, string>)item).Key);
            }

            // Save the Configuration in the Service Configuration file.
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = @"ExternalCameraPickerForWindowsHelloService.exe.config";

            Configuration config =
              ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            config.AppSettings.Settings["devices"].Value = string.Join(";", devices);
            config.Save(ConfigurationSaveMode.Modified);

            // Restart the Service.
            // ExternalCameraPickerForWindowsHelloService
            Debug.Print("Restarting service");
            RestartService("ExternalCameraPickerForWindowsHello", 5000);
        }

        private static void RestartService(string serviceName, int timeoutMilliseconds)
        {
            Debug.Print("Service : {0}", serviceName);
            ServiceController service = new ServiceController(serviceName);
            Debug.Print("Service Status {0}", service.Status);
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
                if (service.Status == ServiceControllerStatus.Running)
                {
                    Debug.Print("Stopping Service..");
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                    Debug.Print("Service Stopped..");
                }
                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                Debug.Print("Starting Service..");
                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                Debug.Print("Service Started..");
            }
            catch
            {
                Debug.Print("Unable to restart Service : {0}", serviceName);
            }
        }
    }
}